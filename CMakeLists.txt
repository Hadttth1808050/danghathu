cmake_minimum_required(VERSION 3.12)
project(baitap2 C)

set(CMAKE_C_STANDARD 99)

add_executable(baitap2 main.c)